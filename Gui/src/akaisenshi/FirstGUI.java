package akaisenshi;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class FirstGUI extends JFrame {
	
	static JLabel text;
	static JButton btn;
	static JTextField tf;

	public FirstGUI() {
		 setLayout(new GridLayout(0,1));

		setVisible(true);
		setSize(800,600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setTitle("Anime List");
		setLayout(null);
		getContentPane().setBackground(Color.DARK_GRAY);

		text = new JLabel("Anime List");
		text.setForeground(Color.LIGHT_GRAY);
		text.setBounds(100, 10 , 250, 35);
		Font schrift = (text.getFont().deriveFont(Font.BOLD + Font.ITALIC, 40));
		text.setFont(schrift);
		add(text);
		
		tf = new JTextField();
		tf.setBackground(Color.LIGHT_GRAY);
		tf.setForeground(Color.DARK_GRAY);
		tf.setBounds(100, 65 , 150, 35);
		tf.setVisible(true);
		tf.setBorder(null);
		add(tf);
		
		btn = new JButton("Add Anime");
		btn.setBackground(Color.gray);
		btn.setForeground(Color.DARK_GRAY);
		btn.setBounds(100, 100, 150, 35);
		btn.setVisible(true);
		btn.addActionListener(new Buttons());
		btn.setBorder(null);
		btn.setFocusPainted(false);
		add(btn);
		Buttons Btn = new Buttons();
		
	}
	

}
