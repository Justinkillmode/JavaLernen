package akaisenshi;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);

		String eingabe = "";
		while (true) {
			System.out.println("\n" + "Was wollen sie berechnen?");
			System.out.println("U f�r Volt");
			System.out.println("I f�r Ampere");
			System.out.println("R f�r Ohm");

			eingabe = sc.nextLine();
			switch (eingabe) {
			case "U":
			case "u":
				Volt.volt();
				break;
			case "i":
			case "I":
				Ampere.ampere();
				break;
			case "r":
			case "R":
				Ohm.ohm();
				break;
			}
		}
	}
}
