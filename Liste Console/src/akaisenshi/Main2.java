package akaisenshi;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.TreeMap;

public class Main2 {

	TreeMap<String, String> map = new TreeMap<>();

	File daten = new File("daten.txt");


	public void showMap() {
		TableList tl = new TableList("Anime","Status").withUnicode(true).sortBy(1);
		for (Map.Entry<String, String> entry : map.entrySet()) {
			String anime = entry.getKey();
			String watched = entry.getValue();
			tl.addRow(anime,watched);
//			System.out.println("| Anime: " + anime + " | " + watched + " |");
		}
		tl.print();
	}

	public void load() throws FileNotFoundException {
		Scanner sca = new Scanner(daten);
		try {
			while (sca.hasNextLine()) {
				String line = sca.next();
				if (line.contains("_"))
					line = line.replaceAll("_", " ");
				map.put(line.substring(0, line.indexOf("|")), line.substring(line.indexOf("|") + 1));
			}
		} catch (NoSuchElementException e) {
		} catch (Exception e1) {
			System.err.println(e1);
		}
		sca.close();
	}

	public void removeFromMap(String toRemove) {
		String tmp = "";
		for (Map.Entry<String, String> entry : map.entrySet()) {
			if (entry.getKey().equalsIgnoreCase(toRemove)) {
				tmp = entry.getKey();
			}
		}
		if (tmp.isEmpty()) {
			System.out.println("Anime nicht in der Liste.");
		}
		map.remove(tmp);
	}

	public void store() throws IOException {
		FileWriter file = new FileWriter(new File("daten.txt"));
		for (Map.Entry<String, String> entry : map.entrySet()) {
			file.append(entry.getKey().replaceAll(" ", "_") + "|" + entry.getValue().replaceAll(" ", "_") + "\n");
		}
		file.flush(); // erst in die File geschrieben
		file.close();
	}

	public void startProgramm() throws IOException {

		load();

		StringBuilder stBuilder = new StringBuilder();
		stBuilder.append("Willkommen in der Anime Liste");
		stBuilder.append("\n");
		stBuilder.append("Waehlen sie einer dieser Funktionen");
		stBuilder.append("\n");
		stBuilder.append("1) Liste Anzeigen");
		stBuilder.append("\n");
		stBuilder.append("2) Anime Einfuegen");
		stBuilder.append("\n");
		stBuilder.append("3) Anime Entfernen");
		stBuilder.append("\n");
		stBuilder.append("4) Exit");

		while (true) { // Programm geht nicht aus
			System.out.println();
			System.out.println(stBuilder);

			Scanner sc = new Scanner(System.in);

			String input = "";
			input = sc.nextLine();

			switch (input) {
			case "1":
				showMap();
				break;
			case "2":
				System.out.println("Geben sie einen Anime ein");
				input = sc.nextLine();
				System.out.println("Anime geguckt? 1) Ja, 2) Nein");
				String input2 = sc.nextLine();

				if (input2.equals("1")) {
					map.put(input, "geguckt");
				} else {
					map.put(input, "nicht geguckt");
				}
				store();
				break;
			case "3":
				System.out.println("Geben sie einen Anime zum loeschen ein.");
				input = sc.nextLine();
				removeFromMap(input);
				store();
				break;
			case "4":
				sc.close();
				System.exit(0);
				break;
			default:
				System.out.println("Error");
			}
		}
	}

	public static void main(String[] args) throws IOException {
		new Main2().startProgramm();
	}
}